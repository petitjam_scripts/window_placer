#!/bin/bash

# https://askubuntu.com/a/240513

xdotool search --class chrome set_desktop_for_window %@ 0

xdotool search --class jetbrains-studio set_desktop_for_window %@ 1

xdotool search --class Code set_desktop_for_window %@ 1

xdotool search --class Gnome-terminal set_desktop_for_window %@ 2

xdotool search --class slack set_desktop_for_window %@ 3
